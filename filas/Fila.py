# encoding: utf-8
'''
Created on 21 de nov de 2016

@author: George Mendonça
'''

class Fila(object):

    def __init__(self):
        self.dados = []
        
    def insere(self, elemento):
        self.dados.append(elemento)
        
    def remove(self):
        return self.dados.pop(0)
    
    def vazia(self):
        return len(self.dados) == 0
    
if __name__ == '__main__':
    fila = Fila()
    fila.insere(-2)
    fila.insere(5)
    fila.insere(10)
    fila.insere(-22)
    print(fila.dados)
    fila.remove()
    fila.remove()
    fila.insere(3)
    print(fila.dados)
    
        