/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estruturadedados.tecnomediajk.opensource; // nome do pacote do projeto;

import java.awt.Font; // importação do recurso de fontes para configuração de Labels e Botões;

/**
 *
 * @author Tadeu Espíndola Palermo
 */
public class TecnoMedia extends javax.swing.JFrame { // classe pública da JFrame Swing (recursos gráficos de janela);

    /**
     * Creates new form TecnoMedia
     */
    public TecnoMedia() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */    
    
    @SuppressWarnings("unchecked") // recursos gráficos criados automaticamente pelo Netbeans;
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtA1 = new javax.swing.JTextField();
        txtA2 = new javax.swing.JTextField();
        btnMedia = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        lblMedia = new javax.swing.JLabel();
        lblStatus = new javax.swing.JLabel();
        lblA3 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtNome = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        lblNome = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        lblMeta = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        lblFaltante = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Bem-vindo(a) ao TecnoMédiaJK v2.0 - Software Livre.");
        setResizable(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Entre com o valor da nota A1:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Entre com o valor da nota A2:");

        btnMedia.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnMedia.setText("Calcular a Média");
        btnMedia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMediaActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Média do aluno:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("Situação em primeira instância:");

        lblMedia.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        lblMedia.setText("0");

        lblStatus.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblStatus.setText("Clique em Calcular a Média para obter o resultado!");

        lblA3.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblA3.setText("Clique em Calcular a Média para obter o resultado!");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel8.setText("Nome do Aluno(a):");

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel9.setText("Aluno(a):");

        lblNome.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblNome.setText("Aguardando...");

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/capelo.jpg"))); // NOI18N

        jLabel10.setFont(new java.awt.Font("Traditional Arabic", 1, 14)); // NOI18N
        jLabel10.setText("TecnoMédiaJK v2.0 - Software Livre");

        jLabel11.setFont(new java.awt.Font("Tahoma", 2, 10)); // NOI18N
        jLabel11.setText("Desenv. Tadeu Espíndola Palermo (copyleft)");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setText("A3:");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setText("Meta:");

        lblMeta.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblMeta.setText("Clique em Calcular a Média para obter o resultado!");

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel12.setText("Observações:");

        lblFaltante.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblFaltante.setText("Clique em Calcular a Média para obter o resultado!");

        jLabel13.setText("((a2 * 2) + a1) / 3");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblMedia, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel4)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(lblStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel6)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(lblA3, javax.swing.GroupLayout.PREFERRED_SIZE, 624, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lblMeta, javax.swing.GroupLayout.PREFERRED_SIZE, 590, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lblFaltante, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(42, 42, 42)))
                                        .addGap(109, 109, 109))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addGroup(layout.createSequentialGroup()
                                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                                            .addComponent(jLabel2)
                                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                            .addComponent(txtA2))
                                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                                            .addComponent(jLabel1)
                                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                            .addComponent(txtA1, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(layout.createSequentialGroup()
                                                            .addGap(87, 87, 87)
                                                            .addComponent(btnMedia))
                                                        .addGroup(layout.createSequentialGroup()
                                                            .addGap(106, 106, 106)
                                                            .addComponent(jLabel13)))
                                                    .addGap(230, 230, 230))
                                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                                    .addComponent(jLabel8)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                    .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, 442, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel9)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(lblNome, javax.swing.GroupLayout.PREFERRED_SIZE, 464, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 6, Short.MAX_VALUE)))
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(23, 23, 23))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addGap(3, 3, 3)
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(txtA1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnMedia, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtA2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel2))
                            .addComponent(jLabel13))
                        .addGap(17, 17, 17)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblNome, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9)))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(lblMedia, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(lblStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(lblA3, javax.swing.GroupLayout.DEFAULT_SIZE, 24, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(lblMeta, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(lblFaltante, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Generated Code é a codificação da Janela principal criada pela IDE Netbeans;
    private void btnMediaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMediaActionPerformed
        // TODO add your handling code here: (Evento para o botão de cálculo da média);
        String nome = (txtNome.getText()); // Variável nome do tipo String, através do método get.Text, recebe o nome do aluno digitado pelo usuário na caixa de texto txtNome;
        float a1 = Float.parseFloat(txtA1.getText()); // Variável a1 do tipo float, através do método Float.parseFloat, converte em número real a nota A1 digitada pelo usuário na caixa de texto txtA1, porém antes recebe a nota A1 digitada pelo usuário na caixa de texto txtA1 através do método get.Text;
        float a2 = Float.parseFloat(txtA2.getText()); // Variável a2 do tipo float, através do método Float.parseFloat, converte em número real a nota A2 digitada pelo usuário na caixa de texto txtA2, porém antes recebe a nota A2 digitada pelo usuário na caixa de texto txtA2 através do método get.Text;
        float med = ((a2 * 2) + a1) / 3; // Variável med do tipo float, recebe o cálculo da média do aluno, multiplicando a2 por peso 2, somando o resultado da multiplicação com o resultado da nota a1 e dividindo o resultado total da soma por 3;
        float faltante7 = 7 - med; // Variável faltante7 do tiplo float, recebe a subtração entre o valor 7 (média precisa) e o valor contido na variável med (média do aluno);
        float sobra = med - 7; // Variável sobra do tipo float, recebe a subtração entre o valor contido na variável med (média do aluno) e o valor 7 (média precisa);
        float faltante3 = 3 - med; // Variável faltante3 do tiplo float, recebe a subtração entre o valor 3 (média mínima para fazer a prova A3) e o valor contido na variável med (média do aluno);
        String status = (med>=7)?"Aprovado!":"Reprovado!"; // Variável status do tipo String, recebe um resultado de comparação lógica (se média foi igual ou maior a 7), através do operador ternário (?), onde se a média do aluno for maior ou igual a 7, o operador retorna a string Aprovado, caso contrário dessa condição, o operador retorna a string Reprovado;
        if (med>=3 && med<7) { // Condição que verifica se a média do aluno é maior ou igual a 3 e média do aluno é menor que 7;
            lblA3.setText("O aluno tem média suficiente para poder fazer a prova A3!"); // Se a condição acima (med>=3 && med<7) for verdadeira,será impresso na tela No Label lblA3, a frase entre aspas duplas que se encontra nesta linha de código, através do método setText que recebe a frase e coloca no Label;
            lblMeta.setText("A meta do aluno é atingir a média 7.0 na prova A3 para ser aprovado!"); // Se a condição acima (med>=3 && med<7) for verdadeira, será impresso na tela no Label lblMeta, a frase entre aspas duplas que se encontra nesta linha de código, através do método setText que recebe a frase e coloca no Label;            
            lblFaltante.setText("Faltou " + (String.format("%.1f",faltante7)) + " de média para o aluno ser aprovado de primeira obtendo a média precisa de 7.0!"); // Se a condição acima (med>=3 && med<7) for verdadeira, será impresso na tela no Label lblFaltante, a frase entre aspas duplas que se encontra nesta linha de código, através do método setText que recebe a frase e coloca no Label. Variável faltante7 formatada com uma casa após a vírgula através do método String.format. A frase utiliza concatenação de string com a variável faltante 7através do operador de adição/concatenação de string (+);                                
        } if (med<3) { // Condição que verifica se a média do aluno é menor que 3;
            lblA3.setText("O aluno não tem média suficiente para poder fazer a prova A3!"); // Se a condição acima (med<3)for verdadeira,será impresso na tela no Label lblA3, a frase entre aspas duplas que se encontra nesta linha de código, através do método setText que recebe a frase e coloca no Label;
            lblMeta.setText("O aluno foi REPROVADO!!!"); // Se a condição acima (med<3) for verdadeira,será impresso na tela no Label lblMeta, a frase entre aspas duplas que se encontra nesta linha de código, através do método setText que recebe a frase e coloca no Label;                        
            lblFaltante.setText("Faltou " + (String.format("%.1f",faltante7)) + " de média para o aluno ser aprovado obtendo média 7.0 e " + (String.format("%.1f",faltante3)) + " para poder fazer a prova A3!"); // Se a condição acima (med<3) for verdadeira, será impresso na tela no Label lblFaltante, a frase entre aspas duplas que se encontra nesta linha de código, através do método setText que recebe a frase e coloca no label. Variáveis faltante7 e faltante3 formatadas com uma casa após a vírgula através do método String.format.  A frase utiliza concatenação de string com a variável faltante7 e faltante3 através do operador de adição/concatenação de string (+);                                 
        } if (med>=7) { // Condição que verifica se a média do aluno é maior ou igual a 7;
            lblA3.setText("O aluno não precisa fazer a prova A3!"); // Se a condição acima (med>=7) for verdadeira, será impresso na tela no Label lblA3 a frase entre aspas duplas que se encontra nesta linha de código, através do método setText que recebe a frase e coloca no Label;            
            lblMeta.setText("O aluno foi APROVADO e atingiu a média " + (String.format("%.1f",med)) + "! Parabéns!"); // Se a condição acima (med>=7) for verdadeira, será impresso na tela no Label lblMeta, a frase entre aspas duplas que se encontra nesta linha de código, através do método setText que recebe a frase e coloca no label. Variável med formatada com uma casa após a vírgula através do método String.format.  A frase utiliza concatenação de string com a variável med através do operador de adição/concatenação de string (+);                               
            lblFaltante.setText("O aluno ultrapassou em " + (String.format("%.1f",sobra)) + " a média precisa para aprovação!!! FENOMENAL!!!"); // Se a condição acima (med>=7) for verdadeira, será impresso na tela no Label lblFaltante, a frase entre aspas duplas que se encontra nesta linha de código, através do método setText que recebe a frase e coloca no label. Variável sobra formatada com uma casa após a vírgula através do método String.format. A frase utiliza concatenação de string com a variável sobra através do operador de adição/concatenação de string (+);                                                                           
        } if (med==7) { // Condição que verifica se a variável med é igual a 7;
            lblFaltante.setText("O aluno foi APROVADO com média precisa de " + (String.format("%.1f",med)) + " !Estude mais!!!"); // Se a condição acima (med==7) for verdadeira, será impresso na tela no Label lblFaltante, a frase entre aspas duplas que se encontra nesta linha de código, através do método setText que recebe a frase e coloca no Label. Variável med formatada com uma casa após a vírgula através do método String.format.  A frase utiliza concatenação de string com a variável med através do operador de adição/concatenação de string (+);                                                                                        
        } if (med>=10) { // Condição que verifica se a variável med é maior ou igual a 10;
            lblFaltante.setText("O aluno foi APROVADO com média " + (String.format("%.1f",med)) + " !!! Parabéns CDF! Você é um exímio vencedor!!!"); // Se a condição acima (med>=10) for verdadeira, será impresso na tela no Label lblFaltante, a frase entre aspas duplas que se encontra nesta linha de código, através do método setText que recebe a frase e coloca no Label. Variável med formatada com uma casa após a vírgula através do método String.format. A frase utiliza concatenação de string com a variável med através do operador de adição/concatenação de string (+);                                                                                        
        }
        lblMedia.setText(String.format("%.1f",med)); // Botão de cálculo da média recebe o cálculo da média que está guardada na variável med, através do método sex.Text. Formatação da variável med com uma casa após a vírgula, através do método String.format;   
        lblMedia.setFont(new Font("Arial Black", Font.PLAIN, 14)); // Formatação da fonte do Label através do método setFont - Recebe o valor gerado pelo cálculo da média do aluno;
        lblNome.setText(nome); // Label que recebe o nome do Aluno digitado pelo usuário e que foi armazenado na variável nome, através do método set.Text;
        lblNome.setFont(new Font("Arial Black", Font.PLAIN, 14)); // Formatação da fonte do Label através do método setFont - Recebe o nome do Aluno digitado pelo usuário na caixa de texto txtNome; 
        lblStatus.setFont(new Font("Arial Black", Font.PLAIN, 14)); // Formatação da fonte do Label lblStatus através do método setFont - Recebe a informação de Aprovado ou Reprovado (Situação em primeira instância) de acordo com a média obtida pelo aluno;
        lblStatus.setText(status); // Label lblStatus que recebe a situação do aluno em primeira instância de acordo com a média obtida pelo aluno, através do método set.Text;
        lblA3.setFont(new Font("Arial Black", Font.PLAIN, 14)); // Formatação da fonte do Label lblA3 através do método setFont - Irá receber a informação que diz se o aluno tem média suficiente, não tem média suficiente ou não precisa fazer a prova A3, através dos métodos set.Text utilizados dentro dos blocos de if (estruturas condicionais);      
        lblMeta.setFont(new Font("Arial Black", Font.PLAIN, 14)); // Formatação da fonte do Label lblMeta através do método setFont - Irá informar sobre as metas de aprovado ou reprovado;
        lblFaltante.setFont(new Font("Arial Black", Font.PLAIN, 14)); // Formatação da fonte do Label lblFaltante através do método setFont - Irá informar a média faltante ou ultrapassada em relação à média precisa;
    }//GEN-LAST:event_btnMediaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TecnoMedia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TecnoMedia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TecnoMedia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TecnoMedia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TecnoMedia().setVisible(true); // verdadeiro para visibilidade
            }
        });
    }
    // A baixo, código gerado pela IDE Netbeans, recursos gráficos, labels, caixas de texto e botão. Biblioteca Swing (Jframe).
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnMedia;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel lblA3;
    private javax.swing.JLabel lblFaltante;
    private javax.swing.JLabel lblMedia;
    private javax.swing.JLabel lblMeta;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblStatus;
    private javax.swing.JTextField txtA1;
    private javax.swing.JTextField txtA2;
    private javax.swing.JTextField txtNome;
    // End of variables declaration//GEN-END:variables
}
