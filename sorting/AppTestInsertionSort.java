/**
 * Implementação de Estruturas de Ordenação - Insertion Sort Test
 * @author George Mendonça
 * Data: 25/09/2016
 * Atualização: 08/11/2016
 * */
package br.com.ed.sorting;

public class AppTestInsertionSort {
	public static void main(String[] args) {
		Sort sl = new Sort(5);
		sl.inserir(20);
		sl.inserir(3);
		sl.inserir(-1);
		sl.inserir(1000);
		sl.inserir(-100);
		sl.exibir();
		sl.selectionSort();
		sl.exibir();
	}
}