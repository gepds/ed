/**
 * Implementação de Estruturas de Ordenação - Selection Sort Test
 * @author George Mendonça
 * Data: 25/09/2016
 * Atualização: 08/11/2016
 * */
package br.com.ed.sorting;

public class AppTestSelectionSort {
	public static void main(String[] args) {
		Sort inSt = new Sort(5);
		inSt.inserir(8);
		inSt.inserir(222);
		inSt.inserir(-50);
		inSt.inserir(-100);
		inSt.inserir(-3);
		inSt.exibir();
		inSt.insertionSort();
		inSt.exibir();
	}
}