/**
 * Criando uma estrutura de dados de lista com objetos Nó
 * @author George Mendonça
 * Data: 14/09/2016
 * Atualização: 27/08/2016
 * */

public class NoTeste{
	public static void main(String[] args) {
		No<String> e1 = new No<String>("George");
		No<String> e2 = new No<String>("Magda");
		No<String> e3 = new No<String>("Hugo Bala");
		
		e1.setProximo(e2);
		e2.setProximo(e3);		
		
		System.out.println("Elemento do priemeiro nó da lista: " + e1.getElemento());
		System.out.println("Elemento do segundo nó da lista: " + e1.getProximo().getElemento());
		System.out.println("Elemento do terceiro nó da lista: " + e1.getProximo().getProximo().getElemento());
		System.out.println(
			"Lista de elementos do tipo String criada: ["+
				e1.getElemento() + ", " + 
				e2.getElemento() + ", " + 
				e3.getElemento() + "]\n...onde cada elemento é um OBJETO, ou seja, um NÓ."
		);
	}
}