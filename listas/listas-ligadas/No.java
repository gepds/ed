/**
 * Implementação do nó de uma estrutura de dados de Lista
 * @author George Mendonça
 * Data: 14/09/2016
 * Atualização:
 * */

public class No<T>{
	private T elemento;
	private No<T> proximo;
	
	public No(T novoElemento) {
		this.elemento = novoElemento;
	}
	
	public No(T novoElemento, No<T> proximoElemeto) {
		this.elemento = novoElemento;
		this.proximo = proximoElemeto;
	}
	
	public T getElemento() {
		return this.elemento;
	}
	
	public void setElemento(T novoElemento) {
		this.elemento = novoElemento;
	}
	
	public No<T> getProximo() {
		return this.proximo;
	}
	
	public void setProximo(No<T> novoProximo) {
		this.proximo = novoProximo;
	}
}
