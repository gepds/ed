/**
 * Criando uma estrutura de dados de lista com objetos Nó
 * @author George Mendonça
 * Data: 28/09/2016
 * Atualização:
 * */

public class NoTeste2{
	public static void main(String[] args) {
		
		No<String> lista = new No<String>("Tadeu");
		No<String> e2 = new No<String>("Hugo");
		No<String> e3 = new No<String>("Suuuu");
		
		lista.setProximo(e2);
		lista.getProximo().setProximo(e3);
		
		System.out.println("Elemento do priemeiro nó da lista: " + lista.getElemento());
		System.out.println("Elemento do segundo nó da lista: " + lista.getProximo().getElemento());
		System.out.println("Elemento do terceiro nó da lista: " + lista.getProximo().getProximo().getElemento());
		System.out.println(
			"Lista de elementos do tipo String criada: ["+
				lista.getElemento() + ", " + 
				e2.getElemento() + ", " + 
				e3.getElemento() + "]\n...onde cada elemento é um OBJETO, ou seja, um NÓ."
		);
	}
}
