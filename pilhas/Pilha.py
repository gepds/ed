# encoding: utf-8
'''
Created on 21 de nov de 2016

@author: George Mendonça
'''

class Pilha(object):

    def __init__(self):
        self.dados = []
        
    def empilha(self, elemento):
        self.dados.append(elemento)
    
    def desempilha(self):
        return self.dados.pop(len(self.dados)-1)     
    
    def vazia(self):
        return len(self.dados) == 0
    
if __name__ == '__main__':
    pilha = Pilha()
    pilha.empilha(-2)
    pilha.empilha(10)
    pilha.empilha(100)
    pilha.empilha(-5)
    print(pilha.dados)
    pilha.desempilha()
    pilha.desempilha()
    pilha.empilha(8)
    pilha.empilha(9)
    print(pilha.dados)   
        