package br.com.ed.pesquisa;
/**
 * Implementação de Estruturas de Pesquisa - Linear Search Test
 * @author George Mendonça
 * Data: 21/11/2016
 * Atualização: 
 * */
public class AppPesquisaLinear {
	public static void main(String[] args) {
		Pesquisa pl = new Pesquisa(5);
		pl.inserir(20);
		pl.inserir(3);
		pl.inserir(-1);
		pl.inserir(1000);
		pl.inserir(-100);
		pl.exibir();
		// TODO Chamar aqui o método "pesquisaLinear"
		//      a ser implementado na classe Pesquisa para progurar
		//      o elemento no array de elementos.

	}
}
