package br.com.ed.pesquisa;
/**
 * Implementação de Estruturas de Pesquisa - Binary Search Test
 * @author George Mendonça
 * Data: 21/11/2016
 * Atualização: 
 * */
public class AppPesquisaBinaria {

	public static void main(String[] args) {
		Pesquisa pb = new Pesquisa(5);
		pb.inserir(20);
		pb.inserir(3);
		pb.inserir(-1);
		pb.inserir(1000);
		pb.inserir(-100);
		pb.exibir();
		// TODO Chamar aqui o método "pesquisaBinaria"
		//      a ser implementado na classe Pesquisa para progurar
		//      o elemento no array de elementos.
	}

}
