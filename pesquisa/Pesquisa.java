package br.com.ed.pesquisa;
/**
 * Implementação de Estruturas de Pesquisa
 * @author George Mendonça
 * Data: 21/11/2016
 * Atualização: 
 */
public class Pesquisa {
	private long[] ar; // Array de elementos

	private int nElementos; // Número de elementos

	/**
	 * Construtor - Define o tamanho da matriz pelo usuário
	 * @param max
	 */
	public Pesquisa(int max) {
		ar = new long[max]; // Cria o array de elementos
		nElementos = 0;
	}
	
	public long[] getAr() {
		return ar;
	}
	
	public void setAr(long[] ar) {
		this.ar = ar;
	}

	/**
	 * Método para adicionar um elemento no final do array
	 * @param valor
	 */
	public void inserir(long valor) { // Adiciona o elemento no final do array
		ar[nElementos] = valor; 	// Insere o elemento
		nElementos++; 			// Incrementa o tamanho do array
	}

	/**
	 * Método para exibir o conteúdo da matriz
	 */
	public void exibir() { // Exibe o conteúdo da matriz
		for (int j = 0; j < nElementos; j++) // para cada elemento,
			System.out.print(ar[j] + " "); // Exibe o elemento
		System.out.println("");
	}

	/**
	 * Algoritmo da Troca de Valores
	 * @param elemento
	 * @param proximoElemento
	 */
	private void trocar(int elemento, int proximoElemento) { // Algoritmo da Troca de Elementos
		long temp = ar[elemento];
		ar[elemento] = ar[proximoElemento];
		ar[proximoElemento] = temp;
	}

}
