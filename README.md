# Pesquisas e estudos em Estrutura de Dados de Programação
## Conteúdo
###### [/tipos-dados] - Implementações sobre Tipos de Dados
###### [/matrizes] - Implementações utilizando a estrurua de dados de Matrizes/Vetores/Arrays
###### [/introducao-oo] - <a href="https://gitlab.com/gepds/poo/tree/master/introducao" target="_blank">Implementações de Introdução à Orientação a Objetos</a>
###### [/listas] - Implementações de estruruas de dados de Lista 
###### [/sorting] - Implementações de estruruas de dados de Ordenação 
###### [/pesquisa] - Implementações de estruruas de dados de Pesquisa 
###### [/pilhas] - Implementações sobre a Estrutura de dados de Pilha
###### [/filas] - Implementações sobre a Estrutura de dados de Fila
###### [/arvores] - Implementações sobre Tipos de Dados
## Projetos
###### [/JK] - Conteúdo dos projetos, pesquisas e trabalhos sobre estruturas de dados realizados com os alunos da Faculdade Jk de Tecnologia
